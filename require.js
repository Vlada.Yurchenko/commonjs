(()=> {
  const cache = {};

  function require(path) {
    if (cache[path]) {
      return cache[path];
    }

    const file = fetchFile(path);
    const anonymFn = new Function('module', 'exports', file);
    const data = {
      exports: {
      }
    };
  
    anonymFn(data, data.exports);

    cache[path] = data.exports;

    return data.exports;
  }

  require.cache = cache;

  window.require = require;

  function fetchFile(filePath){
    const xhr = new XMLHttpRequest();
    xhr.open("GET", filePath , false);
  
    try {
      xhr.send();
    
      return xhr.response;
    } catch(err) { 
      console.error(err);
    }
  }
})();
