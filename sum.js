const sum = (a, b) => a + b;

const pow = (a, b) => a ** b;

exports.sum = sum;
module.exports.pow = pow;
